const http = require('http')
const PORT = process.env.APP_PORT || 8080;

const app = (req, res) => {
  console.log(req.headers);
  res.setHeader('X-App-Header', 'app-header-value');
  res.end('Welcome to App');
}

const server = http.createServer(app);

server.once('error', (err) => {
  console.log('Failed to start server: %s', err);
  cleanup();
  process.exit(1);
});

const cleanup = () => {
  server.close();
};

process
  .once("SIGINT", () => {
    console.log("SIGINT received");
    cleanup();
  })
  .once("SIGTERM", () => {
    console.log("SIGTERM received");
    cleanup();
  })
  .on('unhandledRejection', (reason, p) => {
    console.error(reason, 'Unhandled Rejection at Promise', p);
  })
  .on('uncaughtException', (err) => {
    console.error('Uncaught Exception thrown', err);
    cleanup();
    process.exit(1);
  });

void function start () {
  const listener = server.listen(PORT, (data) => {
    console.log('Server started on port: %d', listener.address().port);
  });
}();
